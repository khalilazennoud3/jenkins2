 def call(Map params =[:]) {
     def settings = params.getOrDefault('settings','settings.xml')
     def DskipTests = params.getOrDefault('DskipTests','false')
 
  
    sh "mvn clean install -DskipTests=${DskipTests} -s resources/${settings}"
  
}
