def call(Map params =[:]){

  def url = params.getOrDefault("url", "http://192.168.43.186:9000")
  def projectkey = params.getOrDefault("projectkey", "demo-lib-app")
  def sonartoken = params.getOrDefault("sonartoken", "2ea2149832e6daed98ed5f94b9dc13aa1c0563e7")
  def settings = params.getOrDefault("settings","settings.xml")

script{withSonarQubeEnv('sonar'){
sh " mvn sonar:sonar -DskipTests -s  resources/${settings} -Dsonar.projectKey=${projectkey} -Dsonar.host.url=${url} -Dsonar.login=${sonartoken}"
  
}
}}
