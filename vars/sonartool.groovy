def call(Map params =[:]){

  def url = params.getOrDefault("url", "http://192.168.43.186:9000")
  def projectkey = params.getOrDefault("projectkey", "demo-lib-app")
  def sonartoken = params.getOrDefault("sonartoken", "fdbac6a846de03164b769415b6d23cfeb708c2f8")
  def sonarserver = params.getOrDefault("sonarserver", "sonar")
  def sonartool = params.getOrDefault("sonartool", "sonarscanner")
  def exclusions = params.getOrDefault("exclusions", "")
  def qualityProfile = params.getOrDefault("qualityProfile", "Sonar way")
  def language = params.getOrDefault("language", "Java")
  def gateName = params.getOrDefault("gateName", "shared-quality-gate")
  def user = params.getOrDefault("user","admin")
  def pass = params.getOrDefault("pass","khalil")



script {
  // choosing quality qualityProfile
  sh"curl -k -X POST  '${url}/projects/api/qualityprofiles/add_project'  --data 'language=${language}&profileName=${qualityProfile}&projectKey=${projectkey}'"
    
  // assign qualitygate

  sh"curl -u '${user}:${pass}' -X POST '${url}/projects/api/qualitygates/select?projectKey=${projectkey}&gateName=${gateName}' "}

    
    

       def scannerHome = tool 'sonarscanner';
           withSonarQubeEnv("sonar") {
           sh "${tool("sonarscanner")}/bin/sonar-scanner  \
           -Dsonar.projectKey=${projectkey} \
           -Dsonar.host.url=${url} \
           -Dsonar.java.binaries=target \
           -Dsonar.exclusions=${exclusions} \
           -Dsonar.login='${sonartoken}'"
               }
// wait for qualitygate
 timeout(time: 1, unit: 'HOURS') {
              def qg = waitForQualityGate()
              if (qg.status != 'OK') {
                  error "Pipeline aborted due to quality gate failure: ${qg.status}"
              }
          }
      
}
